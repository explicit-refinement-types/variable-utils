import Lake
open Lake DSL

package «variable-utils» {
  -- add package configuration options here
}

@[default_target]
lean_lib VariableUtils {
  -- add library configuration options here
}

require std from git "https://github.com/leanprover/std4" @ "main"
require aesop from git "https://github.com/JLimperg/aesop" @ "master"
require mathlib from git "https://github.com/leanprover-community/mathlib4" @ "master"